# JSON Schemas

Custom and generated JSON Schemas that not present in [JSON Schema Store](https://www.schemastore.org/json/).

---

## ArgoCD

- application
- applicationSet
- appProject